package com.cgg.homeworkday6_18.service;

import com.alibaba.fastjson.JSONObject;
import com.cgg.homeworkday6_18.dto.UserShopping;
import com.cgg.homeworkday6_18.dto.UserShoppingExample;
import com.cgg.homeworkday6_18.mapper.UserShoppingMapper;
import com.cgg.homeworkday6_18.utils.RedisUtils;
import com.cgg.homeworkday6_18.vo.UserShoppingVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by CuiGeGe on 2020/6/18
 */
@Service
public class UserShoppingService {

    @Autowired
    private UserShoppingMapper userShoppingMapper;

    @Autowired
    private RedisUtils redisUtils;


    public int regist(String name, String password, String age, String sex) {
        UserShopping userShopping = new UserShopping();
        userShopping.setId(UUID.randomUUID().toString().substring(0, 12));
        userShopping.setAge(age);
        userShopping.setName(name);
        userShopping.setSex(sex);
        userShopping.setPassword(password);
        return userShoppingMapper.insert(userShopping);
    }

    public String login(String name, String password) {
        UserShoppingExample userShoppingExample = new UserShoppingExample();
        userShoppingExample.createCriteria().andNameEqualTo(name).andPasswordEqualTo(password);
        long row = userShoppingMapper.countByExample(userShoppingExample);
        if (row > 0) {
            List<UserShopping> userShopping = userShoppingMapper.selectByExample(userShoppingExample);
            UserShoppingVo userShoppingVo = new UserShoppingVo();
            BeanUtils.copyProperties(userShopping.get(0), userShoppingVo);
            String userStr = JSONObject.toJSONString(userShoppingVo);
            redisUtils.set(userShoppingVo.getId(), userStr);
            return userShoppingVo.getId();
        }
        return null;
    }

    public int loginByName(String name) {
        UserShoppingExample userShoppingExample = new UserShoppingExample();
        userShoppingExample.createCriteria().andNameEqualTo(name);
        long row = userShoppingMapper.countByExample(userShoppingExample);
        if (row > 0) {
            return 1;
        } else {
            return 0;
        }

    }

}
