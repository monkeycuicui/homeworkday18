package com.cgg.homeworkday6_18.controller;

import com.alibaba.fastjson.JSONObject;
import com.cgg.homeworkday6_18.service.UserShoppingService;
import com.cgg.homeworkday6_18.utils.RedisUtils;
import com.cgg.homeworkday6_18.utils.ReturnResult;
import com.cgg.homeworkday6_18.utils.ReturnResultUtils;
import com.cgg.homeworkday6_18.vo.UserShoppingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by CuiGeGe on 2020/6/18
 */
@RestController
@RequestMapping(value = "/usershopping")
public class UserShoppingController {

    @Autowired
    private UserShoppingService userShoppingService;


    @Autowired
    private RedisUtils redisUtils;

    @GetMapping(value = "/regist")
    public ReturnResult regist(String name, String password, String age, String sex) {
        int row = userShoppingService.regist(name, password, age, sex);
        if (row > 0) {
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(400, "注册失败！");
    }


    @GetMapping(value = "/login")
    public ReturnResult login(String name, String password) {

        if (userShoppingService.loginByName(name) == 1) {
            if (userShoppingService.login(name, password) != null) {
                String str = (String) redisUtils.get(userShoppingService.login(name, password));
                UserShoppingVo userShoppingVo = JSONObject.parseObject(str, UserShoppingVo.class);
                return ReturnResultUtils.returnSuccess(userShoppingVo);
            } else {
                return ReturnResultUtils.returnFail(400, "密码错误");
            }
        } else {
            return ReturnResultUtils.returnFail(500, "用户不存在");
        }


    }
}
