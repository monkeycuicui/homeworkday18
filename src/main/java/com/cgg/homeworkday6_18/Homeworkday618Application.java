package com.cgg.homeworkday6_18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homeworkday618Application {

    public static void main(String[] args) {
        SpringApplication.run(Homeworkday618Application.class, args);
    }

}
