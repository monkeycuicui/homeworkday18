package com.cgg.homeworkday6_18.utils;

import lombok.Data;

/**
 * Created by CuiGeGe on 2020/6/17
 */
@Data
public class ReturnResult<T> {
    private int code;
    private String msg;
    private T data;
}
