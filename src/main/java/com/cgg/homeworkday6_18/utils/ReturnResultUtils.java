package com.cgg.homeworkday6_18.utils;

/**
 * Created by CuiGeGe on 2020/6/17
 */
public class ReturnResultUtils {


    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(600);
        returnResult.setMsg("success");
        return returnResult;
    }

    public static <T> ReturnResult returnSuccess(T data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(500);
        returnResult.setMsg("success");
        returnResult.setData(data);
        return returnResult;
    }

    public static ReturnResult returnFail(int code, String msg) {

        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;

    }
}
