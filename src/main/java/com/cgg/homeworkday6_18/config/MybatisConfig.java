package com.cgg.homeworkday6_18.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by CuiGeGe on 2020/6/16
 */
@Configuration
@MapperScan(basePackages = "com.cgg.homeworkday6_18.mapper")
public class MybatisConfig {

}
